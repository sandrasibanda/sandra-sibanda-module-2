void main() {
  var appsof2012 = [
    'FNB Banking APP',
    'HealthID',
    'TransUnion Dealers',
    'RapidTargets',
    'Matchy',
    'Plascon Inspire Me',
    'phraZapp'
  ];
  var appsof2013 = [
    'DSTV',
    '.comm Telco Data Visualizer',
    'PriceCheck Mobile',
    'MarkitShare',
    'Nedbank App Suite',
    'SnapScan',
    'Kids Aid',
    'bookly',
    'Gautrain Buddy'
  ];
  var appsof2014 = [
    'SuperSport',
    'SyncMobile',
    'MyBelongings',
    'Live Inspect',
    'VIGO',
    'Zapper',
    'Rea Vaya',
    'Wildlife Tracker'
  ];
  var appsof2015 = [
    'WumDrop',
    'Vula Mobile',
    'SearchWorks',
    'BoardView Professional',
    'HelloCrowd',
    'humbleTill',
    'DSTV Now',
    'EskomSePush',
    'Sasol Young Explorer Mammals',
    'myToyota',
    'VCPpay',
    'Thumbtom',
    'Standard Online Share Trading',
    'Tuluntulu',
    'PriceCheck',
    'M4JAM',
    'mYnEWsTARRT',
    'Voonja',
    'CPUT Mobile',
    'TouchToLearn',
    'AirSME'
  ];
  var appsof2016 = [
    'Domestly',
    'iKhokha',
    'HearZA',
    'Tuta-me',
    'kaChing',
    'Friendly Match Monster for Kindergarten'
  ];
  var appsof2017 = [
    'Shyft',
    'TransUnion 1Check',
    'OrderIN',
    'EcoSlips',
    'InterGreatMe',
    'Zulzi',
    'Hey Jude',
    'Oru Social',
    'TouchSA',
    'Pick nPay Super Animals 2',
    'The TreeApp South Africa',
    'Watly Health Portal',
    'Awethu Project'
  ];
  var appsof2018 = [
    'PineApple',
    'Cowa Bunga',
    'Digemy KnowledgePartner and Besmarter',
    'Bestee',
    'The African Cyber Gaming League APP',
    'dbTrack',
    'Stokfella',
    'Difela Hymns',
    'Xander English 1-20',
    'Crtl',
    'Khula',
    'ASI Snakes'
  ];
  var appsof2019 = [
    'Naked',
    'SI Realities',
    'Lost Defence',
    'Franc',
    'Vula Mobile',
    'Matric Live',
    'My pregnancy Journal',
    'LocTransie',
    'Hydra',
    'Bottles',
    'Over',
    'Digger',
    'Mo Wash'
  ];
  var appsof2020 = [
    'EasyEquities',
    'Examsta',
    'Checkers Sixty60',
    'Technishen',
    'BirdPro',
    'Lexie Hearing',
    'League of Legends',
    'GreenFingers Mobile',
    'Xitsong Dictionary',
    'Stokfella',
    'Bottles',
    'Matric Live',
    'Guardian Health',
    'My Pregnancy Jounery'
  ];
  var appsof2021 = [
    'Ambani Africa',
    'iiDENTIFii app',
    'HelloPay POS',
    'Guardian Health Platform',
    'Murimi',
    'Shyft',
    'Sisa',
    'UniWise',
    'Kazi',
    'Takealot App',
    'Roadsave',
    'Afrihost'
  ];

  var allapps = [
    appsof2012,
    appsof2013,
    appsof2014,
    appsof2015,
    appsof2016,
    appsof2017,
    appsof2018,
    appsof2019,
    appsof2020,
    appsof2021
  ].expand((x) => x).toList();

  allapps.sort((a, b) => a.toLowerCase().compareTo(b.toLowerCase()));
  print(allapps);
  print("The winning apps for 2017 are $appsof2017");
  print("The winning apps of 2018 are $appsof2018");
  print(allapps.length);
}
