class AppOfTheYear {
  var app;
  var category;
  var developer;
  var year = "2021";

  businessApp() {
    print("The app that won the MTN Business App of the year award is : $app.");
    print("$app won in the $category categories.");
    print("$developer developed the app in the year $year.");
  }
}

void main() {
  var app1 = new AppOfTheYear();
  app1.app = "Ambani Africa App".toUpperCase();
  app1.category =
      "Best Gaming Solution, Best Educational and Best South African Solution";
  app1.developer = "Mukundi Lambani";

  app1.businessApp();
}
